# Flask blog project

## Project dependencies

Run `pip install -r requirements/base.txt`

## Frontend dependencies

Run `npm install` to install bower.
Run `bower install` to install frontend dependencies.

Database:
- flask db init
- flask db migrate
- flask db upgrade
