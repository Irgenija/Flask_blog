import smtplib
from blog import app


def send_email(mail):
    server = smtplib.SMTP('smtp.gmail.com', 587)
    server.starttls()
    server.login(app.config['EMAIL_FOR_SMTP'], app.config['PASSWORD_FOR_SMTP'])

    msg = f"You have been registered. Your login '{mail}'"
    server.sendmail(app.config['DEFAULT_FROM_EMAIL'], mail, msg)
    server.quit()
