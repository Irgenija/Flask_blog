$(document).ready(function() {
    validator = $('#reg_form').validate({
        rules: {
            'email': {
                required: true,
                email: true
            },
            'password': {
                required: true,
                minlength: 8,
                maxlength: 100
            },
            'first_name': {
                required: true,
                maxlength: 80
            },
            'second_name': {
                required: true,
                maxlength: 100
            },
            'nick_name': {
                required: true
            }
        }
    });
});
