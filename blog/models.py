from blog import db


class User(db.Model):
    __tablename__ = 'users'

    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(100), index=True, unique=True)
    password = db.Column(db.String(100))
    first_name = db.Column(db.String(80))
    second_name = db.Column(db.String(100))
    nick_name = db.Column(db.String(128))


class Article(db.Model):
    __tablename__ = 'articles'

    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String, index=True)
    author = db.Column(db.String(200), index=True)
    date = db.Column(db.Date, index=True)
    text = db.Column(db.String)
