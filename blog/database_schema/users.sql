drop table if exists users;
create table users (
  id integer primary key autoincrement,
  email varchar(100) not null,
  password varchar(100) not null,
  first_name varchar(80) not null,
  second_name varchar(100) not null,
  nick_name text not null
);
