drop table if exists articles;
create table articles (
  id integer primary key autoincrement,
  title text not null,
  author text not null,
  date text not null,
  text text not null
);
