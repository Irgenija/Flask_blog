from flask import request, session, redirect, url_for, \
    render_template, flash
from blog import app

from blog import db
from blog.models import User, Article
from blog.forms import LoginForm, AddArticleForm, SearchForm, RegForm

import hashlib

from blog.emails import send_email


def get_articles():
    """
    Get articles from DB
    :return:
    """
    result = Article.query.all()
    return result


@app.route('/')
def show_articles():
    """
    Index view
    :return:
    """
    form = AddArticleForm(request.form)
    return render_template('show_articles.html', articles=get_articles(), form=form)


@app.route('/add', methods=['GET', 'POST'])
def add_article():
    """
    Add article view
    :return:
    """
    form = AddArticleForm(request.form)

    if not session.get('logged_in'):
        return redirect(url_for('login'), code=301)

    if form.validate():
        article = Article(title=form.title.data,
                          author=form.author.data,
                          date=form.date.data,
                          text=form.text.data
                          )
        db.session.add(article)
        db.session.commit()
        flash('New article was successfully posted.')
        return redirect(url_for('show_articles'))
    else:
        articles = get_articles()
        return render_template('show_articles.html', articles=articles, form=form)


@app.route('/login', methods=['GET', 'POST'])
def login():
    """
    Login view
    :return:
    """
    form = LoginForm(request.form)

    if session.get('logged_in'):
        return redirect(url_for('show_articles'), code=301)

    if request.method == 'POST' and form.validate():
        session['logged_in'] = True
        flash('You were logged in')
        return redirect(url_for('show_articles'))
    return render_template('login.html', form=form)


@app.route('/logout')
def logout():
    """
    Logout view
    :return:
    """
    session.pop('logged_in', None)
    flash('You were logged out')
    return redirect(url_for('show_articles'))


@app.route('/registration', methods=['GET', 'POST'])
def registration():
    """
    Registration view
    :return:
    """
    if session.get('logged_in'):
        return redirect(url_for('show_articles'))

    form = RegForm(request.form)
    if request.method == 'POST' and form.validate():
        password = form.password.data.strip().encode("utf-8")
        hash_password = hashlib.sha224(password).hexdigest()
        user = User(email=form.email.data,
                    password=hash_password,
                    first_name=form.first_name.data,
                    second_name=form.second_name.data,
                    nick_name=form.nick_name.data
                    )
        db.session.add(user)
        db.session.commit()
        flash('New user was successfully registered.')
        send_email(request.form['email'])
        return redirect(url_for('login'))

    return render_template('registration.html', form=form)


@app.route('/search', methods=['GET', 'POST'])
def search():
    """
    Search view
    :return:
    """
    form = SearchForm(request.form)

    if not session.get('logged_in'):
        return redirect(url_for('login'), code=301)

    if request.method == 'POST' and form.validate():
        # Search articles in DB
        search_result = Article.query.filter_by(title=form.title.data)
        return render_template('search.html', articles=search_result, form=form)

    return render_template('search.html', form=form)
