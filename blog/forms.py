import datetime

from wtforms import Form, StringField, PasswordField, validators
from wtforms.validators import DataRequired
from wtforms.widgets import TextArea

from blog.models import User
import hashlib


class LoginForm(Form):
    """
    Login form to blog
    """
    login = StringField('Login', validators=[DataRequired()])
    password = PasswordField('Password', validators=[DataRequired()])

    def validate(self):
        validation_result = Form.validate(self)
        if not validation_result:
            return False

        result = User.query.filter_by(email=self.login.data)

        if result:
            password_db = User.query.filter_by(email=self.login.data).first()
            if password_db:
                password = self.password.data.strip().encode("utf-8")
                hash_password = hashlib.sha224(password).hexdigest()
                if password_db.password != hash_password:
                    self.password.errors.append('Invalid password')
                    return False
        else:
            self.login.errors.append('Invalid login.')
            return False
        return True


class RegForm(Form):
    """
    Registration form to blog
    """
    email = StringField('Email', validators=[DataRequired()])
    password = PasswordField('Password', validators=[DataRequired()])
    first_name = StringField('First Name', validators=[DataRequired()])
    second_name = StringField('Second Name', validators=[DataRequired()])
    nick_name = StringField('Nickname', validators=[DataRequired()])

    def validate_email(self, field):
        result = User.query.filter_by(email=field.data)
        if result:
            raise validators.ValidationError('This email is already registration.')


class AddArticleForm(Form):
    """
    Add articles to blog
    """
    title = StringField('Title', validators=[DataRequired()])
    author = StringField('Author', validators=[DataRequired()])
    date = StringField('Date', validators=[DataRequired()])
    text = StringField('Text', validators=[DataRequired()], widget=TextArea())

    def validate_date(self, field):
        try:
            datetime.datetime.strptime(field.data, '%d-%m-%Y')
        except ValueError:
            raise validators.ValidationError('Incorrect data format, should be DD-MM-YYYY.')


class SearchForm(Form):
    """
    Search articles
    """
    title = StringField('Title', validators=[DataRequired()])
